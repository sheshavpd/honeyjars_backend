const Docker = require('dockerode');
const docker = new Docker({host: '127.0.0.1', port: 2375});


async function testDocker() {
    const containers = await docker.listContainers({ all: true }, undefined);
    console.log(containers);
}

testDocker();
