const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const routes = require("./routes");

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '12mb', extended: true}));

const port = process.env.PORT || 3000;
app.use('/api', routes);

app.listen(port, () => console.log(`Listening on *:${port}`));
