const routes = require('express').Router();
const remotes = require('./remotes');
const docker = require('./docker');

routes.use((req, res, next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,token,email,docHash');
    res.setHeader('Access-Control-Allow-Credentials', true);
    if(req.method === 'OPTIONS')
        return res.status(200).send();
    next();
});

routes.use('/remotes', remotes);
routes.use('/docker', docker);
module.exports = routes;
