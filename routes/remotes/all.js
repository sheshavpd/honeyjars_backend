const remotes = require("../../store/remotes");

const allRemotes = async function(req, res) {
    const allRemotes = [];
    for(let i = 0; i < remotes.allRemotes.length; i++) {
        const remote = remotes.allRemotes[i];
        const remoteContainers = await remote.docker.listContainers({ all: true });
        allRemotes.push({
            host: remote.host,
            port: remote.port,
            containers: remoteContainers
        });
    }

    res.json({error: false, allRemotes});
}

module.exports = allRemotes;
