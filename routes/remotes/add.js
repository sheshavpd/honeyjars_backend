const { validateParams } = require("../../utils/Helpers");
const remotes = require("../../store/remotes");

const validate = function(req, res) {
    if(!validateParams([req.body.host, req.body.port])) {
        res.json({error: true, message: "Invalid Params"});
        return false;
    }
    return true;
}

const addRemote = function(req, res) {
    if(!validate(req, res)) {
        return;
    }
    const remote = remotes.addRemote(req.body.host, req.body.port);
    if(!remote) {
        res.json({error: true, message: "Remote already exists."});
        return;
    }
    res.json({error: false, message: "Remote added."});
}

module.exports = addRemote;
