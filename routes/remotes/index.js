const remotes = require('express').Router();
const add = require('./add');
const all = require('./all');

remotes.post('/add', add);
remotes.get('/all', all);

module.exports = remotes;
