// serviceInfo should be of format:
// {name: "SomeName", properties: [ {name: "..", value: ".."} ], configs: [ {name: "..", properties: [ {name: "..", value: ".." } ] } ] }
module.exports = function (serviceInfo) {
    let serviceStr = `[service.${serviceInfo.name}]\n`;
    for (const prop of serviceInfo.properties) {
        serviceStr = serviceStr.concat(`${prop.name}=${JSON.stringify(prop.value)}\n`);
    }

    let serviceConfigStr = ``;
    for (const config of serviceInfo.configs) {
        serviceConfigStr = serviceConfigStr.concat(`[[${config.name}]]\n`);
        for (const prop of config.properties) {
            serviceConfigStr = serviceConfigStr.concat(`${prop.name}=${JSON.stringify(prop.value)}\n`);
        }
    }

    return `[listener]
type="socket"

${serviceStr}

${serviceConfigStr}

[channel.console]
type="console"

[[filter]]
channel=["console"]

[[logging]]
output = "stdout"
level = "info"`;
}
