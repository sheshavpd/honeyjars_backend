const serviceConfigBuilder = require("./serviceConfigBuilder");
const remotes = require("../../../store/remotes");
const path = require("path");
const fs = require("fs");

// Provide service in the format: { name: "..", hostPort: "..", containerPort: "..",  serviceConfig: {..} }
const create = async function (req, res) {
    const docker = remotes.findRemote(req.body.host, req.body.port).docker;
    if (!docker) {
        res.json({error: true, message: "No such remote exists."});
        return;
    }
    const serviceConfig = JSON.parse(req.body.serviceConfig);
    const serviceConfStr = serviceConfigBuilder(serviceConfig);
    const containerName = `${req.body.name}_${serviceConfig.name}`;
    const serviceConfFilePath = path.join(__dirname, "configs", `${containerName}.toml`);
    if (serviceConfFilePath.includes(" ")) {
        res.json({
            error: true,
            message: "The path where the backend is hosted contains a white space. Please ensure there are no white spaces in the path of backend."
        });
        return;
    }

    try {
        fs.writeFileSync(serviceConfFilePath, serviceConfStr);
        const dockerContainer = await docker.createContainer({
            name: containerName,
            ExposedPorts: {
                [`${req.body.containerPort}`]: {}
            },
            Env: ["ENABLE_CORS=true"],
            HostConfig: {
                Binds: [`${serviceConfFilePath}:/config.toml`],
                PortBindings: {[`${req.body.containerPort}`]: [{"HostPort": `${req.body.hostPort}`}]}
            },
            Cmd: [
                "--config", "/config.toml"
            ],
            Image: "honeytrap/honeytrap:latest"
        });
        await dockerContainer.start();
        console.log("Created new container");
    } catch (e) {
        res.json({error: true, message: e.message});
        return;
    }

    res.json({error: false, message: "Created Service"});
};

const safeCreate = async function (req, res) {
    try {
        await create(req, res);
    } catch (e) {
        console.log(e);
        res.json({error: true, message: "Invalid configurations."});
    }
}

module.exports = safeCreate;
