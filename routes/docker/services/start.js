const remotes = require("../../../store/remotes");
const start = async function(req, res) {
    const docker = remotes.findRemote(req.body.host, req.body.port).docker;
    if (!docker) {
        res.json({error: true, message: "No such remote exists."});
        return;
    }
    if(!req.body.Id) {
        res.json({error: true, message: "Invalid params"});
        return;
    }

    try {
        await (await docker.getContainer(req.body.Id)).start();
        res.json({error: false});
    } catch(e) {
        console.log(e);
        res.json({error: true, message: "Server Error"});
    }
};

module.exports = start;
