const services = require('express').Router();
const create = require('./create');
const stop = require('./stop');
const start = require('./start');
const del = require('./delete');

services.post('/create', create);
services.post('/stop', stop);
services.post('/start', start);
services.post('/delete', del);

module.exports = services;
