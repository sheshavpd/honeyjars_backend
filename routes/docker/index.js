const docker = require('express').Router();
const services = require('./services');

docker.use('/services', services);

module.exports = docker;
