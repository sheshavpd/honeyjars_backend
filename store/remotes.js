const path = require("path");
const fs = require("fs");
const Docker = require("dockerode");

const allRemotesPath = path.join(__dirname, "remotes.txt");

class Remote {
    constructor(host, port) {
        this.host = host;
        this.port = port;
    }

    async initialize() {
        this._docker = new Docker({host: this.host, port: this.port});
    }

    get docker() {
        return this._docker;
    }
}

class Remotes {
    allRemotes = [];
    constructor() {
        if(fs.existsSync(allRemotesPath)) {
            const remoteAddresses = JSON.parse(fs.readFileSync(allRemotesPath, 'utf-8'));
            remoteAddresses.forEach(async address => {
                await this.addRemote(address.host, address.port);
            });
        }
    }

    async addRemote(host, port) {
        //Remote with same host and port already exists.
        if(this.findRemote(host, port)) {
            return false;
        }
        const remote = new Remote(host, port);
        await remote.initialize();
        this.allRemotes.push(remote);
        fs.writeFileSync(allRemotesPath, JSON.stringify(this.allRemotes.map(remote => {
            return {host: remote.host, port: remote.port};
        })));
        return remote;
    }

    findRemote(host, port) {
        const remIndex = this.allRemotes.findIndex(el => el.host === host && el.port === port);
        if(remIndex === -1) {
            return false;
        }
        return this.allRemotes[remIndex];
    }
}

const remotes = new Remotes();
module.exports = remotes;
